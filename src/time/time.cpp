#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <sys/time.h> // gettimeofday
#include <string.h> // strerror
#include <errno.h> // errno
#include <unistd.h>

// c++
// #include <chrono>
#include <iostream>
#include <string>

#define USED_CLOCK CLOCK_MONOTONIC_RAW // CLOCK_MONOTONIC_RAW if available
#define NANOS 1000000000LL

using namespace std;

// typedef long long ll;
// typedef unsigned long long ull;

void get_time(struct timespec* t) {
   if (clock_gettime(USED_CLOCK, t)) {
      exit(EXIT_FAILURE);
   }
}

// returns 1 if different between first and second is > 'sec' seconds
// (i.e. timer has expired), otherwise returns 0
bool check_timer_expired(double sec, struct timespec* first, struct timespec* second) {
   int64_t elapsed, microseconds;
   elapsed = (second->tv_sec * NANOS + second->tv_nsec) - (first->tv_sec * NANOS + first->tv_nsec);
   microseconds = elapsed / 1000 + (elapsed % 1000 >= 500); // round up halves

   if ((sec * 1000000) < microseconds)
      return true;
   return false;
}

struct timeval get_timestamp() {
   struct timeval tv;
   if (gettimeofday(&tv, NULL) == -1) {
      printf("Error, get_timestamp returned error with %s\n", strerror(errno));
   }
   return tv;
}

uint64_t get_timestamp_microsec() {
   struct timeval tv;
   if (gettimeofday(&tv, NULL) == -1) {
      printf("Error, get_timestamp returned error with %s\n", strerror(errno));
   }
   return tv.tv_sec * 1000000ull + tv.tv_usec;
}

// e.g. Wed Mar  1 22:48:54 2017
// NOTE: can modified to write to debug log
void print_timestamp_asctime(uint64_t microseconds) {
   time_t tt = (time_t)(microseconds / 1000000ull);
   printf("%s", asctime(localtime(&tt)));
}

// e.g. 01-03-2017 22:48:54
// NOTE: can modified to write to debug log
void print_timestamp_formatted(uint64_t microseconds) {
   char buffer[80];
   struct tm *timeinfo;
   time_t tt = (time_t)(microseconds / 1000000ull);
   timeinfo = localtime(&tt);
   strftime(buffer, 80, "%d-%m-%Y %H:%M:%S", timeinfo);
   string str(buffer);
   cout << str << endl;  
}


int main(int argc, char *argv[]) {

   
   // timeval t1, t2;
   uint64_t t1, t2;
   t1 = get_timestamp_microsec();
   
   printf("starting timer...\n");   
   // get initial time
   struct timespec begin, current;
   get_time(&begin);
   get_time(&current);

   // how long we want the timer to last
   double diff = 1.0;
   printf("waiting for timer to expire in %f seconds\n", diff);
   while (!check_timer_expired(diff, &begin, &current)) {
      get_time(&current);
   }
   printf("timer has expired.\n\n");

   t2 = get_timestamp_microsec();

   print_timestamp_asctime(t1);
   print_timestamp_asctime(t2);

   print_timestamp_formatted(t1);
   print_timestamp_formatted(t2);

   
   return EXIT_SUCCESS;
}

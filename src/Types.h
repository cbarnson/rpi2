

#ifndef TYPES_H_
#define TYPES_H_


#include <stdint.h>

// MessageHeader::message_type
const uint8_t HELLO_MESSAGE = 1;
const uint8_t TC_MESSAGE = 2;
const uint8_t MID_MESSAGE = 3;
const uint8_t HNA_MESSAGE = 4;

// link types
const uint8_t UNSPEC_LINK = 0; 	// 0000 XX00
const uint8_t ASYM_LINK = 1;	// 0000 XX01
const uint8_t SYM_LINK = 2;   	// 0000 XX10
const uint8_t LOST_LINK = 3;	// 0000 XX11

// neighbor types
const uint8_t NOT_NEIGH = 0; 	// 0000 00XX
const uint8_t SYM_NEIGH = 1; 	// 0000 01XX
const uint8_t MPR_NEIGH = 2; 	// 0000 10XX
const uint8_t UNSPEC_NEIGH = 3; // I added this, not in standard

// for neighbor_tuple N_status
const uint8_t NOT_SYM = 0;
const uint8_t SYM = 1;

// willingness
const uint8_t WILL_NEVER = 0;
const uint8_t WILL_LOW = 1;
const uint8_t WILL_DEFAULT = 3;
const uint8_t WILL_HIGH = 6;
const uint8_t WILL_ALWAYS = 7;


#endif /* TYPES_H_ */

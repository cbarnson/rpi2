

#ifndef TOPOLOGYSET_H_
#define TOPOLOGYSET_H_

#include "Shared.h"

struct topology_tuple {
	uint32_t T_dest_addr; // main address of a node reachable in one hop from the node with main addr T_last_addr
	uint32_t T_last_addr; // is mpr of T_dest_addr
	uint32_t T_seq;
	uint64_t T_time; // time this tuple expires and MUST be removed
};

class TopologySet {
public:
	TopologySet();
	~TopologySet();

private:
	list<topology_tuple> topology_set;
};

#endif /* TOPOLOGYSET_H_ */

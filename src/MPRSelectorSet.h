

#ifndef MPRSELECTORSET_H_
#define MPRSELECTORSET_H_

#include "Shared.h"

struct mpr_selector_tuple {
	uint32_t MS_main_addr; // main address of a node which has selected this node as mpr
	uint32_t MS_time; // time at which the tuple expires and MUST be removed
};

class MPRSelectorSet {
public:
	MPRSelectorSet();
	~MPRSelectorSet();

private:
	list<mpr_selector_tuple> mpr_selector_set;
};


#endif /* MPRSELECTORSET_H_ */

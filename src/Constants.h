
#ifndef CONSTANTS_H_
#define CONSTANTS_H_

// max transmission unit (bytes)
const int MTU = 1500;

// emission intervals
const double HELLO_INTERVAL = 2.0;    // seconds
const double REFRESH_INTERVAL = 2.0;  // seconds
const double TC_INTERVAL = 5.0;       // seconds
const double MID_INTERVAL = TC_INTERVAL;
const double HNA_INTERVAL = TC_INTERVAL;

// holding times
const double NEIGHB_HOLD_TIME = 3 * REFRESH_INTERVAL;
const double TOP_HOLD_TIME = 3 * TC_INTERVAL;
const double DUP_HOLD_TIME = 30.0;    // seconds
const double MID_HOLD_TIME = 3 * MID_INTERVAL;
const double HNA_HOLD_TIME = 3 * HNA_INTERVAL;

// for computing validity time "vtime" and "htime" fields
const double SCALING_FACTOR = 0.0625; // seconds

const int MESSAGE_HEADER_SIZE = 12; // bytes
const int PACKET_HEADER_SIZE = 4;
const int MINIMUM_PACKET_SIZE = 16; // if packet_length is < this value, silently discard
const int HELLO_HEADER_SIZE = 4;

const int MAXIMUM_PACKET_PAYLOAD = MTU - PACKET_HEADER_SIZE;
const int MAXIMUM_MESSAGE_PAYLOAD = MTU - (MESSAGE_HEADER_SIZE + PACKET_HEADER_SIZE);
const int MAXIMUM_HELLO_PAYLOAD = MTU - (PACKET_HEADER_SIZE + MESSAGE_HEADER_SIZE + HELLO_HEADER_SIZE);

#endif /* CONSTANTS_H_ */



#ifndef MESSAGEHEADER_H_
#define MESSAGEHEADER_H_

#include "Shared.h"
#include "Database.h"

struct message_header {
   uint8_t message_type;
   uint8_t vtime;
   uint16_t message_size;
   uint32_t originator_address;
   uint8_t ttl;
   uint8_t hop_count;
   uint16_t message_sequence_number;
   uint8_t message_data[MAXIMUM_MESSAGE_PAYLOAD]; // 1484
};

class MessageHeader {
  public:
   MessageHeader();
   MessageHeader(uint8_t buf[MAXIMUM_PACKET_PAYLOAD], int&);
   ~MessageHeader();

   void ProcessMessage(uint8_t buf[MAXIMUM_PACKET_PAYLOAD], int&, Database&);

   void ClearHeader();
   void PrintHeader();

  private:

   message_header header;
   double vtimef;

};

#endif /* MESSAGEHEADER_H_ */



#ifndef DEBUGLOG_H_
#define DEBUGLOG_H_

#include "Shared.h"

class DebugLog {
  public:
   ~DebugLog();
   static DebugLog* Instance();
   void Write(string str);
   void WriteTimestamp(string str);
   void WriteAddress(uint32_t addr);
   
   void OpenFile();
   void CloseFile();

   bool IsOpen();
   
  private:
   static bool instance_flag;
   static DebugLog *single;
   DebugLog();
   
   FILE *flog = NULL;
};


#endif /* DEBUGLOG_H_ */

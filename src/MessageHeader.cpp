

#include "MessageHeader.h"
#include "HelloMessage.h"
#include "Types.h"
#include "Helper.h"



MessageHeader::MessageHeader() {
}

MessageHeader::MessageHeader(uint8_t buf[MAXIMUM_PACKET_PAYLOAD], int& i) {

   // get header info
   header.message_type = buf[i++];
   header.vtime = buf[i++];
   header.message_size = (buf[i] << 8) | (buf[i+1]); i += 2;
   header.originator_address = (buf[i] << 8*3) | (buf[i+1] << 8*2) |
      (buf[i+2] << 8) | buf[i+3]; i += 4;
   header.ttl = buf[i++];
   header.hop_count = buf[i++];
   header.message_sequence_number = (buf[i] << 8) | buf[i+1]; i += 2;

   // convert network byte order to host
   header.message_size = ntohs(header.message_size);
   header.originator_address = ntohl(header.originator_address);
   header.message_sequence_number = ntohs(header.message_sequence_number);

   // get vtime as a floating point number
   vtimef = vtime_to_double(header.vtime);

   printf("message type: %i\n", (int)header.message_type);
   
}

MessageHeader::~MessageHeader() {
}

// i is the index of the packet data (spans multiple messages), important
// that this value is incremented with each byte we take out of buf
void MessageHeader::ProcessMessage(uint8_t buf[MAXIMUM_PACKET_PAYLOAD], int& i,
   Database& db) {

   // using message size, copy to message_data, update i
   int size = header.message_size - MESSAGE_HEADER_SIZE;
   assert(size >= 0);

   // copy "size" bytes of buf, starting from i, into message_data
   for (int j = 0; j < size; j++) {
      header.message_data[j] = buf[i++];
   }
   // NOW DON'T CHANGE "i" ANYMORE, WE HAVE EVERYTHING WE NEED FROM CALLER'S BUFFER

   // check message originator address, if matches this nodes main address
   // do not process message (don't process messages sent by self)

   // DuplicateSet calls here

   // first call RemoveExpired() to get rid of invalid entries if they exist

   // then call EntryExists(...)
   // if D_addr == header.originator_addr AND
   // D_seq_num == header.message_sequence_number
   // already processed, do not process again
   // else,
   // add entry to DuplicateSet with InsertEntry(...)

   // FORWARDING CONDITION GOES HERE
   // === NOT REQUIRED FOR NEIGHBOR DISCOVERY PART
   // if D_addr == originator addr, AND D_seq_num == message sequence number, AND
   // the receiving interface (address) is in D_iface_list
   // then do NOT forward (retransmit)
   // SEE DEFAULT FORWARDING ALGORITHM PG 18 RFC 3626

	
	
   // if is a hello message, create a HelloMessage, pass control off to that object
   if (header.message_type == HELLO_MESSAGE) {
      HelloMessage hm(header.message_data, size, header.vtime,
		      header.originator_address);
      hm.ProcessHelloMessage(db);
   }

   // ADD CONDITIONS FOR OTHER MESSAGE TYPES HERE

}

void MessageHeader::ClearHeader() {
   assert(sizeof(message_header) == MAXIMUM_PACKET_PAYLOAD);
   memset(&header, 0, sizeof(message_header));
}

void MessageHeader::PrintHeader() {
   cout << (int)header.message_type << endl;
   cout << (int)header.vtime << endl;
   cout << (int)header.message_size << endl;
   cout << (int)header.originator_address << endl;
   cout << (int)header.ttl << endl;
   cout << (int)header.hop_count << endl;
   cout << (int)header.message_sequence_number << endl;
}



#include "PacketHeader.h"
#include "MessageHeader.h"
#include <arpa/inet.h>


PacketHeader::PacketHeader() {
}

PacketHeader::PacketHeader(void* buf, int buflen) {
   assert(buflen <= MTU);
   assert(MTU == sizeof(packet_header));

   // destination, source, size
   // memcpy(&header, buf, buflen);

   memcpy(&header, buf, 4);
   //PrintHeader();

   printf("packet length %i, ", (int)header.packet_length);
   printf("packet seq num %i\n", (int)header.packet_sequence_number);
   
   // convert to host byte order
   header.packet_length = ntohs(header.packet_length);
   header.packet_sequence_number = ntohs(header.packet_sequence_number);

   printf("PacketHeader(), packet length: %i", (int)header.packet_length);
   printf(", packet seq number: %i\n", header.packet_sequence_number);
   
}


PacketHeader::~PacketHeader() {
}

// process a received packet, header information has already been created in 
// constructor, so proceed as defined by OLSR algorithm in RFC 3626
void PacketHeader::ProcessPacket(Database& db) {
   if (!IsValidLength()) return;

   // progress through the entire packet
   while (dataIndex < (header.packet_length - 4)) {

      //cout << "dataIndex " << dataIndex << endl;
      MessageHeader mh(header.packet_data, dataIndex);
      //cout << "dataIndex " << dataIndex << endl;
      mh.ProcessMessage(header.packet_data, dataIndex, db);

   }

}

void PacketHeader::ClearHeader() {
   assert(sizeof(packet_header) == PACKET_HEADER_SIZE);
   memset(&header, 0, sizeof(packet_header));
}

void PacketHeader::PrintHeader() {
   cout << (int)header.packet_length << endl;
   cout << (int)header.packet_sequence_number << endl;
}

// signals to discard the packet if length is less that the min number of bytes
// (16 for IPv4)
bool PacketHeader::IsValidLength() {
   return !(header.packet_length < MINIMUM_PACKET_SIZE);
}


#ifndef SHARED_H_
#define SHARED_H_


// C headers
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <wchar.h>
#include <assert.h>
#include <signal.h>
#include <time.h>
#include <sys/time.h> // gettimeofday
#include <string.h> // strerror
#include <errno.h> // errno
#include <unistd.h>
//#include <cstring>
#include <arpa/inet.h>

// C++ headers
#include <iostream>
#include <list>
#include <string>
#include <iterator>
#include <bitset>
#include <iomanip>
#include <cmath>

// other
#include "Constants.h"
#include "Types.h"

using namespace std;

// defines
#ifndef USED_CLOCK
#define USED_CLOCK CLOCK_MONOTONIC_RAW // CLOCK_MONOTONIC_RAW if available
#endif

#ifndef NANOS
#define NANOS 1000000000LL
#endif

#endif /* SHARED_H_ */

#ifndef HELLOMESSAGE_H
#define HELLOMESSAGE_H

#include "Shared.h"
#include "Database.h"

struct hello_message_header {
   uint16_t reserved;
   uint8_t htime;
   uint8_t willingness;
   uint8_t hello_data[MAXIMUM_HELLO_PAYLOAD];
};

class HelloMessage {
  public:
   HelloMessage(uint8_t buf[MAXIMUM_HELLO_PAYLOAD], int message_size, uint8_t vtime,
      uint32_t originator_address);
   ~HelloMessage();

   void ProcessHelloMessage(Database&);
   bool IsHelloExhausted();

  private:
   hello_message_header header;
   uint8_t message_vtime;
   uint32_t originator_address;

   // bytes stored in hello_data, this includes everything from the first
   // "link code" to the last "neighbor interface address"
   int data_size = 0;
   int data_index = 0;

   // true if link type and neighbor type are valid
   bool IsLinkCodeValid(uint8_t, uint8_t);

};

#endif

#include "HelloMessage.h"


HelloMessage::HelloMessage(uint8_t buf[MAXIMUM_HELLO_PAYLOAD], int message_size,
			   uint8_t vtime, uint32_t originator) {
   // fill info in hello_message_header member
   // here, we start from index 0, since the "buf" parameter is really the message
   // data from MessageHeader object
   // NOTE: in this context, message_size does NOT include the 12 bytes of
   // message header
   int i = 0;
   header.reserved = (buf[i] << 8) | buf[i+1]; i += 2;
   header.htime = buf[i++];
   header.willingness = buf[i++];

   // network to host byte order
   header.reserved = ntohs(header.reserved);

   // store rest of provided data in hello_data
   assert(i < message_size);
   int j = 0;
   for (i; i < message_size; i++) {
      header.hello_data[j++] = buf[i];
   }
   // now we have everything we will ever need 

   // save message size in data_size member for processing multiple link/neighbor
   // types
   data_size = message_size;

   // get vtime from this message
   message_vtime = vtime;

   // get originator from this message
   originator_address = originator;
}

HelloMessage::~HelloMessage() {
   // TODO Auto-generated destructor stub
}

// at this point, we have the hello message "header data"
// i.e. Reserved, Htime, Willingness
// this message will process through the 'next' logical link/neighbor type list
// which is stored in header.hello_data and consists (total) of data_size bytes
void HelloMessage::ProcessHelloMessage(Database& db) {

   // while there is a link/neighbor type left to process
   while (!IsHelloExhausted()) {
      int j = data_index;
      uint8_t link_code = header.hello_data[j++];
      uint8_t reserved = header.hello_data[j++];
      uint16_t link_message_size = (header.hello_data[j] << 8) |
	 header.hello_data[j+1]; j += 2;

      // link code
      // if link code value is <= 15, must interpret as hold two different 
      // fields, of 2 bits each
      uint8_t link_mask = 3; 		// 0000 0011
      uint8_t neigh_mask = 12; 	// 0000 1100
      uint8_t link_type = (link_code & link_mask);
      uint8_t neigh_type = (link_code & neigh_mask) >> 2;

      // network to host byte order
      link_message_size = ntohs(link_message_size);

      // length of list of neighbor interface addresses
      int size = link_message_size - 4;
      // number of entries
      int count = size / 4;

      // entries start at header.hello_data[j] and are 4 bytes long
      for (int k = 0; k < count; k++) {
	 uint32_t neighbor_interface_addr = 
	    (header.hello_data[j] << 8*3) | (header.hello_data[j+1] << 8*2) |
	    (header.hello_data[j+2] << 8) | header.hello_data[j+3];
	 // update j
	 j += 4;

	 // network to host byte order
	 neighbor_interface_addr = ntohl(neighbor_interface_addr);

	 // do this here so indexes update correctly
	 if (!IsLinkCodeValid(link_type, neigh_type)) {
	    continue;
	 }

	 // pass control to local information database class to process
	 // the details associated with this link code and neighbor interface
	 // address
	 db.ProcessHelloMessage(link_type, neigh_type, neighbor_interface_addr,
				message_vtime, originator_address);

	 // ==========================
	 // process with this neighbor interface addr
	 // ==========================
      }

      // update data_index with j value
      data_index = j;

   } // end of while

}

// check for invalid link/neigh type
bool HelloMessage::IsLinkCodeValid(uint8_t ltype, uint8_t ntype) {
   // neighbor type is not one of the 3 defined neighbor types
   if (ntype < 0 || ntype > 2) return false;
   if (ltype < 0 || ltype > 3) return false;
   if (ltype == SYM_LINK && ntype == NOT_NEIGH) return false;
   return true;
}

// returns true if our logical 'point' of processing this hello message has 
// completed processing for all link/neighbor types, else returns false
bool HelloMessage::IsHelloExhausted() {
   // must have min 4 bytes remaining for there to be a valid list
   return (data_size - data_index) < 4;
}

#include "Database.h"
#include "Helper.h"
#include "DebugLog.h"

Database::Database(uint32_t source_addr) {
   source_main_address = source_addr;
}

Database::~Database() {

}


void Database::ProcessHelloMessage(uint8_t ltype, uint8_t htype, uint32_t addr,
				   uint8_t message_vtime, uint32_t originator) {

   DebugLog *dlog;
   dlog = DebugLog::Instance();
   dlog->Write("In Database, ProcessHelloMessage\n");
   
   // do not process
   if (originator == source_main_address) {
      dlog->Write("originator is self, do not process\n");
      return;
   }

   // if originator is main address of a L_neighbor_iface_addr from a link
   // tuple, included in link set, with
   // l_sym_time >= current time, (is a symmetric neighbor)
   // then two hop neighbor set should be updated

   for (auto i = link_set.begin(); i != link_set.end(); ++i) {
      if (i->L_neighbor_iface_addr == originator &&
	  i->L_SYM_time >= get_timestamp_microsec()) {
	 TwoHopUpdate(ltype, htype, addr, message_vtime, originator);
      }
   }
     
   
   // first, clear expired messages (or done in process?)
   
   // if no link tuple with addr exists
   if (!LinkExists(addr)) {
      dlog->Write("Inserting address to link set\n");
      LinkInsert(addr, message_vtime);
   } else {
      dlog->Write("Link set entry exists, modifying entry\n");
      LinkModify(addr, message_vtime, ltype);
   }
   
}

// if originator is main address of a L_neighbor_iface_addr from a link
// tuple, included in link set, with
// l_sym_time >= current time, (is a symmetric neighbor)
// then two hop neighbor set should be updated 
void Database::TwoHopUpdate(uint8_t link_type, uint8_t neigh_type,
			    uint32_t neigh_addr, uint8_t vtime,
			    uint32_t originator) {
   int presize = two_hop_set.size();
   // remove if node is not its own 2 hop neighbor
   two_hop_set.remove_if([&](const two_hop_tuple& tht) {
	 return ((neigh_type == SYM_NEIGH) ||
		 (neigh_type == MPR_NEIGH) &&
		 (tht.N_neighbor_main_addr == source_main_address)); }
      );
   int postsize = two_hop_set.size();

   if (postsize == presize) {
      // the "else" part of our remove condition
      two_hop_tuple entry;
      entry.N_neighbor_main_addr = originator;
      entry.N_2hop_addr = neigh_addr;      
      double vtimef = vtime_to_double(vtime);            
      entry.N_time = get_timestamp_microsec() + vtimef * 1000000ull;
      // TODO: this entry may replace older similar tuple with same
      // N_neighbor_main_addr and N_2hop_addr values
      two_hop_set.push_back(entry);
   }   
   
}

void Database::LinkModify(uint32_t addr, uint8_t vtime, uint8_t ltype) {
   // find the link

   for (auto it = link_set.begin(); it != link_set.end(); ++it) {
      if (it->L_neighbor_iface_addr == addr) {

	 // warning vtime
	 double vtimef = vtime_to_double(vtime);	 
	 it->L_ASYM_time = get_timestamp_microsec() + vtimef * 1000000ull;
         // confirm this is ok
	 it->L_time = max(it->L_time, it->L_ASYM_time);
	 
      }
   }
   
}

void Database::NeighInsert(link_tuple lt) {
   neighbor_tuple nt;
   nt.N_neighbor_main_addr = lt.L_neighbor_iface_addr;
   nt.N_status = NOT_SYM;
   nt.N_willingness = WILL_DEFAULT;
   NeighUpdate(lt);
}

void Database::NeighUpdate(link_tuple lt) {
   uint64_t curr_time = get_timestamp_microsec();   
   for (auto i = neighbor_set.begin(); i != neighbor_set.end(); ++i) {      
      if (i->N_neighbor_main_addr == lt.L_neighbor_iface_addr) {
	 if (lt.L_SYM_time >= curr_time) {
	    i->N_status = SYM;
	 } else {
	    i->N_status = NOT_SYM;
	 }	 
      }      
   }
}

// each time a link is deleted, the associated neighbor tuple must be removed
// if it has no longer any associated link tuples
void Database::NeighRemove(uint32_t addr) {
   neighbor_set.remove_if([&] (const neighbor_tuple& nt)
			  { return addr == nt.N_neighbor_main_addr; });
}

void Database::LinkInsert(uint32_t addr, uint8_t vtime) {

   uint64_t curr_time = get_timestamp_microsec();
   
   link_tuple entry;
   entry.L_local_iface_addr = source_main_address;
   entry.L_neighbor_iface_addr = addr;
   assert(curr_time > 1000000);
   entry.L_SYM_time = curr_time - 1000000ull; // expired

   uint64_t vtime_usec = (uint64_t)vtime * 1000000ull;
   entry.L_time = curr_time + vtime_usec;

}

bool Database::LinkExists(uint32_t addr) {
   for (auto it = link_set.begin(); it != link_set.end(); ++it) {
      if (it->L_neighbor_iface_addr == addr)
	 return true;
   }
   return false;
}





void Database::Generate(uint32_t caller_addr, list<generate_tuple>& payload) {

   printf("db generate\n");
   
   
   // get time
   uint64_t curr_time = get_timestamp_microsec();
   
   // for each tuple in link_set   
   list<link_tuple>::iterator i;
   for (i = link_set.begin(); i != link_set.end(); ++i) {

      if (i->L_local_iface_addr == caller_addr &&
	  i->L_time >= curr_time) {

	 // link code
	 uint8_t ltype;
	 uint8_t ntype;
	 uint8_t link_code = 0;
	 
	 // link type
	 if (i->L_SYM_time >= curr_time) {
	    ltype = SYM_LINK;
	 } else if (i->L_ASYM_time >= curr_time && i->L_SYM_time < curr_time) {
	    ltype = ASYM_LINK;
	 } else {
	    assert(i->L_ASYM_time < curr_time);
	    assert(i->L_SYM_time < curr_time);
	    ltype = LOST_LINK;
	 }

	 // neighbor type
	 ntype = GetNeighborType(i->L_neighbor_iface_addr);

         // no corresponding match was found in neighbor_set
	 if (ntype == UNSPEC_NEIGH) {
	    ltype = UNSPEC_LINK;
	 }

	 // create the entry with computed fields
	 generate_tuple entry;
	 entry.link_code = (ltype | (ntype << 2));
	 entry.neighbor_interface_address = i->L_neighbor_iface_addr;

	 // add to list
	 payload.push_back(entry);
	 
      }
      
   }
   
}

uint8_t Database::GetNeighborType(uint32_t L_neighbor_iface_addr) {

   list<neighbor_tuple>::iterator i;
   for (i = neighbor_set.begin(); i != neighbor_set.end(); ++i) {

      // if argument is included in this set
      if (i->N_neighbor_main_addr == L_neighbor_iface_addr) {

	 uint8_t ntype;
	 if (i->N_status == SYM) {
	    ntype = SYM_NEIGH;
	 } else {
	    assert(i->N_status == NOT_SYM);
	    ntype = NOT_NEIGH;
	 }
	 return ntype;
	 
      }
      
   }
   return UNSPEC_NEIGH;
}

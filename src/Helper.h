

#ifndef HELPER_H_
#define HELPER_H_


#include "Shared.h"

static void get_time(struct timespec* t) {
   if (clock_gettime(USED_CLOCK, t)) {
      exit(EXIT_FAILURE);
   }
}

// returns 1 if different between first and second is > 'sec' seconds
// (i.e. timer has expired), otherwise returns 0
static bool check_timer_expired(double sec, struct timespec* first,
				struct timespec* second) {
   int64_t elapsed, microseconds;
   elapsed = (second->tv_sec * NANOS + second->tv_nsec) -
      (first->tv_sec * NANOS + first->tv_nsec);
   microseconds = elapsed / 1000 + (elapsed % 1000 >= 500); // round up halves

   if ((sec * 1000000) < microseconds)
      return true;
   return false;
}

static struct timeval get_timestamp() {
   struct timeval tv;
   if (gettimeofday(&tv, NULL) == -1) {
      printf("Error, get_timestamp returned error with %s\n", strerror(errno));
   }
   return tv;
}

static uint64_t get_timestamp_microsec() {
   struct timeval tv;
   if (gettimeofday(&tv, NULL) == -1) {
      printf("Error, get_timestamp returned error with %s\n", strerror(errno));
   }
   return tv.tv_sec * 1000000ull + tv.tv_usec;
}

// e.g. Wed Mar  1 22:48:54 2017
// NOTE: can modified to write to debug log
static void print_timestamp_asctime(uint64_t microseconds) {
   time_t tt = (time_t)(microseconds / 1000000ull);
   printf("%s", asctime(localtime(&tt)));
}

static string str_timestamp_asctime(uint64_t microseconds) {
   time_t tt = (time_t)(microseconds / 1000000ull);
   return asctime(localtime(&tt));
}

// e.g. 01-03-2017 22:48:54
// NOTE: can modified to write to debug log
static void print_timestamp_formatted(uint64_t microseconds) {
   char buffer[80];
   struct tm *timeinfo;
   time_t tt = (time_t)(microseconds / 1000000ull);
   timeinfo = localtime(&tt);
   strftime(buffer, 80, "%d-%m-%Y %H:%M:%S", timeinfo);
   string str(buffer);
   cout << str << endl;  
}


// helper functions
static double vtime_to_double(uint8_t t) {
   int b = (int)(bitset<4>(t).to_ulong());
   int a = (int)(t >> 4);
   // to be safe, pow unpredictable...
   int bb = 1;
   for (int i = 0; i < b; i++)
      bb *= 2;
   return (SCALING_FACTOR * (1.0 + ((double)a / 16)) * (double)bb);
}


static uint8_t double_to_vtime(double t) {
   assert(t >= 0);
   unsigned int b = 0;
   double tc_ratio = t / SCALING_FACTOR;
   while (tc_ratio >= pow(2, b)) {
      b++;
   }
   // want largest that satisfies the above, decrement once
   b--;
   // compute 16*(T/(C*(2^b))-1) and round up, this is 'a'
   double temp = ceil(16.0 * (t / (SCALING_FACTOR * ((double)pow(2, b))) - 1.0));
   unsigned int a = temp;
   // if 'a' is equal to 16, increment 'b' by one, and set 'a' to 0
   if (a == 16) {
      b++;
      a = 0;
   }
   // check
   assert(a >= 0 && a < 16);
   assert(b >= 0 && b < 16);

   unsigned char c = 0;
   c |= (a << 4);
   c |= b;
   return c;
}


#endif /* HELPER_H_ */

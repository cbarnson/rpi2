#ifndef DATABASE_H
#define DATABASE_H

#include "Shared.h"

struct generate_tuple {
   uint8_t link_code;
   uint32_t neighbor_interface_address;   
};

// info used when declaring the neighbor interfaces in the HELLO messages
struct link_tuple {
   uint32_t L_local_iface_addr;	        
   uint32_t L_neighbor_iface_addr;

   // if L_SYM_time >= current time (not expired, is symmetric)
   uint64_t L_SYM_time;

   // if L_SYM_time < current time, and L_ASYM_time >= current time
   // (not expired, is asymmetric)
   uint64_t L_ASYM_time;
   uint64_t L_time;
};

// info describing neighbors
struct neighbor_tuple {
   uint32_t N_neighbor_main_addr; // main address of a neighbor
   uint8_t N_status; // specifies if NOT_SYM or SYM
   uint8_t N_willingness; // willingness to carry traffic on behalf of other nodes
};

struct two_hop_tuple {
   uint32_t N_neighbor_main_addr; // main address of the neighbor
   uint32_t N_2hop_addr; // main address of a 2-hop neighbor with symmetric
                         // link to N_neighbor_main_addr
   uint64_t N_time; // time at which the tuple expires and MUST be removed
};

class Database {

  public:
   Database(uint32_t);
   ~Database();


   void ProcessHelloMessage(uint8_t link_type, uint8_t neigh_type,
			    uint32_t neigh_addr, uint8_t vtime,
			    uint32_t originator);

   bool LinkExists(uint32_t);
   void LinkInsert(uint32_t neigh_addr, uint8_t vtime);
   void LinkModify(uint32_t neigh_addr, uint8_t vtime, uint8_t ltype);
   
   void NeighInsert(link_tuple lt);
   void NeighUpdate(link_tuple lt);
   void NeighRemove(uint32_t addr);

   void TwoHopUpdate(uint8_t link_type, uint8_t neigh_type,
		     uint32_t neigh_addr, uint8_t vtime,
		     uint32_t originator); 
   
   void Generate(uint32_t, list<generate_tuple>&);

  private:
   // keeps info about the neighbors, (complements the link set);
   // node is a neighbor of another node if and only if there is
   // at least one link between the two nodes
   list<neighbor_tuple> neighbor_set;

   // links to each neighbor node, has associated status
   // "symmetric", verified to be bi-directional
   // , or "asymmetric", hello messages from the node have been heard
   // but not confirmed that this node is able to receive messages as well
   list<link_tuple> link_set;

   // two hop neighbor local information database
   list<two_hop_tuple> two_hop_set;


   uint32_t source_main_address;// node whom this database belongs to (local)

   // functions
   uint8_t GetNeighborType(uint32_t L_neighbor_iface_addr);
   
   
};


#endif 

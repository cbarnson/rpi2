/*
 * main.cpp
 *
 *  Created on: 2017-02-28
 *      Author: Cody Barnson
 */
// C
#include <sys/socket.h>
#include <sys/types.h>
#include <netpacket/packet.h>
#include <net/ethernet.h>
#include <unistd.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <stdint.h>

// C++ 
#include <algorithm>
#include <map>
#include <iomanip>

// other
#include "Shared.h"
#include "Database.h"
#include "Types.h"
#include "DebugLog.h" // debug info to file
#include "Helper.h" // vtime, time functions
#include "PacketHeader.h"
#include "MessageHeader.h"
#include "HelloMessage.h"
#include "Sock.h"
// not required for now
// #include "MPRSelectorSet.h"
// #include "TopologySet.h"

uint16_t MY_MESSAGE_SEQUENCE_NUMBER = 101;
uint16_t MY_PACKET_SEQUENCE_NUMBER = 234;
const uint8_t MY_ADDRESS_FIRST_BYTE = 0x41;

void address_to_string(uint32_t addr) {
  uint8_t arr[4];
  arr[3] = addr & 0xff;
  arr[2] = (addr >> 8) & 0xff;
  arr[1] = (addr >> 16) & 0xff;
  arr[0] = (addr >> 24) & 0xff;
   
  for (int i = 0; i < 3; i++)
    printf("%02X:", arr[i]);
  printf("%02X\n", arr[3]);
}

void handle_receive(Database& db) {
  struct Ifconfig ifc;
  init(&ifc);

  // get my address
  uint8_t my_addr[4];   
  my_addr[0] = MY_ADDRESS_FIRST_BYTE;
  my_addr[1] = ifc.mac[3];
  my_addr[2] = ifc.mac[4];
  my_addr[3] = ifc.mac[5];
  uint32_t my_main_addr = (my_addr[0] << 8*3) | (my_addr[1] << 8*2) |
    (my_addr[2] << 8) | (my_addr[3]);
  // print address
  printf("Listening on ");
  address_to_string(my_main_addr);
   
  /* setting up the socket address structure for the source of the packet */
  struct sockaddr_ll from;
  socklen_t fromlen = sizeof(from);
   
  // what is the MTU, tested, is 1500, assert here
  assert(ifc.mtu == 1500);
   
  /* setting up the buffer or receiving */
  char * buf = (char*)malloc(ifc.mtu);
  if (buf == NULL)
    die("Cannot allocate receiving buffer\n");

  // set to non blocking
  // fcntl(ifc.sockd, F_SETFL, fcntl(ifc.sockd, F_GETFL) | O_NONBLOCK);

  // RECEIVE
  int recvlen = recvfrom(ifc.sockd, buf, ifc.mtu, 0,
                         (struct sockaddr*) &from, &fromlen);
  // nothing received
  if (recvlen < 0) {
    printf("Cannot receive data: %s\n", strerror(errno));
    return;
  } else {

    printf("\n============RECEIVING %d BYTES\n==============\n", recvlen);
    
    void *ptr = NULL;
    ptr = buf;

    uint8_t temp[5] = { 0 };
    memcpy(temp, ptr, 5);

    uint8_t msg_type = temp[4];
    if (msg_type == HELLO_MESSAGE) {
      

      DebugLog *dlog;
      dlog = DebugLog::Instance();
      dlog->Write("Node at address ");
      dlog->WriteAddress(my_main_addr);      
      dlog->Write("receiving %d bytes of HELLO message\n");
      
      // process packet
      PacketHeader ph((void*)buf, recvlen);
      ph.ProcessPacket(db);      
    }
     
     
  }
  /* free resources */
  free(buf);
  destroy(&ifc);
}

struct link_code_header {
  uint8_t link_code;
  uint8_t reserved;
  uint16_t link_message_size;
};

void handle_send(Database& db) {

  struct Ifconfig ifc;
  init(&ifc);
   
  // get my address
  uint8_t my_addr[4];
  my_addr[0] = MY_ADDRESS_FIRST_BYTE;
  my_addr[1] = ifc.mac[3];
  my_addr[2] = ifc.mac[4];
  my_addr[3] = ifc.mac[5];
  uint32_t my_main_addr = (my_addr[0] << 8*3) | (my_addr[1] << 8*2) |
    (my_addr[2] << 8) | (my_addr[3]);
  // print address
  printf("Sending from ");
  address_to_string(my_main_addr);

  // Use database to generate a list of neighbor interface addresses, each
  // of a given link code
  list<generate_tuple> glist;
  db.Generate(my_main_addr, glist);


  // buffer to hold data inside Hello message
  uint8_t hello_data[MAXIMUM_HELLO_PAYLOAD];
  int index = 0;

  // count frequency of each link code, use to determine link message size
  map<int, int> freqmap;
  // organize each address in multimap by link code as the key
  multimap<uint8_t, uint32_t> mmap;
   
  for_each(glist.begin(), glist.end(), [&] (const generate_tuple& gt) {
      mmap.insert(
                  pair<uint8_t, uint32_t>(gt.link_code, gt.neighbor_interface_address));
      freqmap[(int)gt.link_code] += 1;
      return;
    });


  // fill hello message buffer with info we got from database generate, then
  // organized with map and multimap structures
  int total_hello_data_size = 0;   
  for (auto it = freqmap.begin(); it != freqmap.end(); ++it) {
    uint8_t link_code = it->first;
    uint16_t link_message_size = (it->second * 4) + 4;

    // update total for checking purposes
    total_hello_data_size += (int)link_message_size;
      
    link_code_header lch;
    lch.link_code = link_code;
    lch.reserved = 0;
    lch.link_message_size = htons(link_message_size);
    assert(sizeof(link_code_header) == 4);
    memcpy(hello_data + index, &lch, sizeof(link_code_header));

    // update index
    index += sizeof(link_code_header);
      
    // get all addresses in mmap of this linkcode
    auto range = mmap.equal_range(link_code);
    for (auto jt = range.first; jt != range.second; ++jt) {
      uint32_t addr = jt->second;
      addr = htonl(addr);
      memcpy(hello_data + index, &addr, sizeof(uint32_t));
      // update index
      index += 4;
    }
    // confirm indices are correct
    assert(total_hello_data_size == index);      
  }
  // complete, now hello_data from 0 to "index" - 1 contains the
  // payload for this hello packet

  /* set-up destination structure in a sockaddr_ll struct */
  struct sockaddr_ll to;
  memset(&to, 0, sizeof(to)); /* clearing the structure, just in case */
  to.sll_family = AF_PACKET; /* always AF_PACKET */
  to.sll_ifindex = ifc.ifindex;
  to.sll_halen = MACADDRLEN;

  /* setup broadcast address of FF:FF:FF:FF:FF:FF */
  int i;
  for (i=0; i<MACADDRLEN; i++)
    to.sll_addr[i] = 0xff;

   
  // ==========packet_header==========
  // NOTE: set packet_length below
  packet_header ph;
  ph.packet_sequence_number = htons(MY_PACKET_SEQUENCE_NUMBER++);
  fill(ph.packet_data, ph.packet_data+(MTU-4), 0);
  // =================================


   
   
  // ==========message_header==========
  message_header mh;
  mh.message_type = HELLO_MESSAGE;
  mh.vtime = double_to_vtime(NEIGHB_HOLD_TIME);
  // 12 for message header, 4 for hello message header, index for hello data length
  int message_size = MESSAGE_HEADER_SIZE + HELLO_HEADER_SIZE + index;
  mh.message_size = htons(message_size);   
  mh.originator_address = htonl(my_main_addr);
  mh.ttl = 1;
  mh.hop_count = 0;
  mh.message_sequence_number = htons(MY_MESSAGE_SEQUENCE_NUMBER++);
  // ==================================



   
  // ==========hello_message_header==========
  // copy buffer data into a hello message header struct
  hello_message_header hmh;
  hmh.reserved = 0;
  hmh.htime = 0; // do not compute this
  hmh.willingness = WILL_DEFAULT; // always set to this
  copy(hello_data, hello_data + index, hmh.hello_data);
  // ========================================

   
  // copy from hello message header stuct to message header struct
  void *p = mh.message_data;
  memcpy((void*)p, &hmh, sizeof(hmh)); // if issue, try 4 + index for copy length
   
  // copy from message header to packet
  int packet_size = PACKET_HEADER_SIZE + message_size;
  ph.packet_length = htons(packet_size); // set packet length
  p = ph.packet_data;
  memcpy((void*)p, &mh, sizeof(mh)); // if issue, try mh.message_size for length

  char msg[MTU] = { 0 };
  memcpy(msg, &ph, sizeof(ph)); // if issue, try ph.packet_length for length
  // also can try (for sending length), MTU, or sizeof(ph)
  int sentlen = sendto(ifc.sockd, msg, packet_size, 0,
                       (struct sockaddr*) &to, sizeof(to));
  printf("%d bytes sent\n", sentlen);

  destroy(&ifc);
   
}

int main() {


  DebugLog *dlog;
  dlog = DebugLog::Instance();
  dlog->Write("Log file opened for main node\n");

  // pi 4, 1; last 3 bytes of mac
  uint32_t my_main_addr = 0;
  // local information base
  Database local_database(my_main_addr);	   
	
  int loop = 10;
  while (loop--) {
    // for send hello interval
    struct timespec begin, current;
    get_time(&begin);
    get_time(&current);
      
    while (!check_timer_expired(HELLO_INTERVAL, &begin, &current)) {
	 
      struct timespec b, c;
      get_time(&b);
      get_time(&c);	 
      while (!check_timer_expired(0.05, &b, &c)) {
        get_time(&c);
      }
      // do receive
      printf("RECEIVE\n");
      handle_receive(local_database);	 
      get_time(&current);
    }
    // do send
    printf("SEND\n");
    handle_send(local_database);
  }

  dlog->CloseFile();
  return 0;
}


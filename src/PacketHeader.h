

#ifndef PACKETHEADER_H_
#define PACKETHEADER_H_

#include "Shared.h"
#include "Database.h"

struct packet_header {
   uint16_t packet_length;
   uint16_t packet_sequence_number;
   uint8_t packet_data[MAXIMUM_PACKET_PAYLOAD]; // max minus the 4 bytes of header
};

class PacketHeader {
  public:
   PacketHeader();
   PacketHeader(void*, int);
   ~PacketHeader();

   void ProcessPacket(Database& );

   void ClearHeader();
   void PrintHeader();
   bool IsValidLength();

  private:
   packet_header header;
   int dataIndex = 0;

	
};

#endif /* PACKETHEADER_H_ */


#include "DebugLog.h"
#include "Helper.h"

bool DebugLog::instance_flag = false;
DebugLog* DebugLog::single = NULL;

DebugLog::DebugLog() {
   OpenFile();
}

DebugLog* DebugLog::Instance() {
   if (!DebugLog::instance_flag) {
      DebugLog::single = new DebugLog();
      DebugLog::instance_flag = true;
      return DebugLog::single;
   } else {
      return DebugLog::single;
   }
}

DebugLog::~DebugLog() {
   instance_flag = false;
   CloseFile();
   // fclose(flog);
   // printf("\nLog file closed\n");
}

void DebugLog::Write(string str) {
   if (flog == NULL) {
      OpenFile();
   }
   assert(flog);
   fprintf(flog, "%s", str.c_str());
}

void DebugLog::WriteTimestamp(string str) {
   uint64_t usec = get_timestamp_microsec();
   string ts = str_timestamp_asctime(usec);

   if (flog == NULL) {
      OpenFile();
   }
   assert(flog);
   fprintf(flog, "%s\t%s", ts.c_str(), str.c_str());
}

void DebugLog::WriteAddress(uint32_t addr) {
  if (flog == NULL) {
    OpenFile();    
  }
  assert(flog);
  uint8_t arr[4];
  arr[3] = addr & 0xff;
  arr[2] = (addr >> 8) & 0xff;
  arr[1] = (addr >> 16) & 0xff;
  arr[0] = (addr >> 24) & 0xff;
   
  for (int i = 0; i < 3; i++)
    fprintf(flog, "%02X:", arr[i]);
  fprintf(flog, "%02X ", arr[3]);
}

bool DebugLog::IsOpen() {
   if (flog == NULL)
      return false;
   else
      return true;
}

void DebugLog::OpenFile() {
   if (flog == NULL) {
      flog = fopen("rpi.log", "w");
      assert(flog);
      printf("Log file opened successfully\n");
   } else {
      printf("Log file already open\n");
   }
}

void DebugLog::CloseFile() {
   if (flog == NULL) {
      printf("Log file already closed\n");
   } else {
      fclose(flog);
      flog = NULL;
      printf("Log file closed successfully\n");
   }
}

